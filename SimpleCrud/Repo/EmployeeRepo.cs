﻿using Dapper;
using SimpleCrud.Data;
using SimpleCrud.Entities;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
namespace SimpleCrud.Repo
{
    public interface IEmployeeRepo
    {
        public Task<IEnumerable<Employee>> GetEmployees();

        public Task<Employee> GetEmployee(int ID);
        public Task<bool> Save(Employee emp);
    }
    public class EmployeeRepo : IEmployeeRepo
    {
        private readonly Context _context;
        public EmployeeRepo(Context context)
        {
            _context = context;
        }

        public async Task<bool> Save(Employee emp)
        {
            bool result = false;
            var param = new DynamicParameters();
            string qry = "";
            if (emp.EmployeeID > 0)
            {
                qry = "Update Employee set FullName=@FullName,Address=@Address,CellPhone=@CellPhone,Country=@Country where EmployeeID=@EmployeeID";
                param.Add("@EmployeeID", emp.EmployeeID);
            }
            else
            {
                qry = "Insert into Employee(FullName,Address,CellPhone,Country) Values(@FullName,@Address,@CellPhone,@Country)";
            }
            param.Add("@FullName", emp.FullName);
            param.Add("@Address", emp.Address);
            param.Add("@CellPhone", emp.CellPhone);
            param.Add("@Country", emp.Country);
            using (var connection = _context.CreateConnection())
            {

                var rowEffected = await connection.ExecuteAsync(qry, param);

                result = true;
            }
            return result;
        }

        public async Task<Employee> GetEmployee(int id = 0)
        {
            string qry = "select * from Employee where EmployeeID=@EmployeeID";
            var param = new DynamicParameters();
            param.Add("@EmployeeID", id);
            using (var connection = _context.CreateConnection())
            {
                var employee = await connection.QuerySingleOrDefaultAsync<Employee>(qry, new { EmployeeID = id });
                return employee != null ? employee : new Employee { EmployeeID = 0 };
            }
        }

        public async Task<IEnumerable<Employee>> GetEmployees()
        {
            string qry = "select * from Employee";
            using (var connection = _context.CreateConnection())
            {
                var employees = await connection.QueryAsync<Employee>(qry);
                return employees.ToList();
            }
        }
    }
}

﻿namespace SimpleCrud.Entities
{
    public class Employee
    {
        public int EmployeeID { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string CellPhone { get; set; }

    }
}

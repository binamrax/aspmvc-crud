﻿using Microsoft.AspNetCore.Mvc;
using SimpleCrud.Repo;
using SimpleCrud.Entities;

namespace SimpleCrud.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeRepo _employeeRepo;

        public EmployeeController(IEmployeeRepo employeeRepo)
        {
            _employeeRepo = employeeRepo;
        }
        public async Task<IActionResult> Index()
        {
            ViewBag.Message = TempData["Message"];
            var emp = await _employeeRepo.GetEmployees();
            return View(emp);
        }

        public async Task<IActionResult> Save(int id = 0)
        {
            if (id > 0)
            {
                var emp = await _employeeRepo.GetEmployee(id);
                if (emp.EmployeeID == 0)
                {
                    return RedirectToAction("Index");
                }

                return View(emp);
            }
            return View();
        }

        [HttpPost]
        public IActionResult Save(Employee emp)
        {
            _employeeRepo.Save(emp);
            TempData["Message"] = "Saved Sucessfully.";
            return RedirectToAction("Index");
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using SimpleCrud.Repo;
using System.Diagnostics;
using System.Net;
namespace SimpleCrud.Controllers
{
    public class APIController : Controller
    {
        private readonly IEmployeeRepo _employeeRepo;
        public APIController(IEmployeeRepo employeeRepo) 
        {
            _employeeRepo = employeeRepo;
        }
        public IActionResult Index()
        {
            return Json(new { Sucess=true});
        }
        [Route("api/employee")]
        public async Task<IActionResult> Employee()
        {
            var emp = await _employeeRepo.GetEmployees();
            return Json(emp.ToList());
        }
        [Route("api/employee/{id}")]
        public async Task<IActionResult> Employee(int id)
        {
            var emp = await _employeeRepo.GetEmployee(id);
            if (emp.EmployeeID > 0)
            {
                return Json(emp);
            }
            else
            {
                Response.StatusCode = 404;
                return Json("ID not found");
            }
        }

    }
}
